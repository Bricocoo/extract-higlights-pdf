# extract-higlights-pdf

## Name
extract-higlights-pdf

## Description
This python script extracts highlighted text in a PDF and creates a txt document with the highlights of all the PDF files in the folder, plus the name and author of each PDF file, and the page of the highlighted text of the PDF.

## Installation
The script needs several python packages to be installed: os, sys, PyMuPDF

`pip install PyMuPDF`

## Usage
Once the script is cloned, copy it in the FOLDER where your highlighted PDF files are stored. Then open a terminal and :

`cd FOLDER`

`python3 extract-highlight_def.py`

The script creates a file highlights_output.txt taht contains the highlighted texts in all the PDF files of the folder, plus the name and author of the PDF files, and the page of the extracted text.

## Support
Please email me at cbrissaud@gmail.com for support.

## License
MIT License

## Project status
Very beginning
