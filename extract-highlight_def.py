import os
import sys
from typing import List, Tuple
import re
import fitz

# Function to parse highlighted text
def _parse_highlight(annot: fitz.Annot, wordlist: List[Tuple[float, float, float, float, str, int, int, int]]) -> str:
    points = annot.vertices
    quad_count = int(len(points) / 4)
    sentences = []
    for i in range(quad_count):
        # where the highlighted part is
        r = fitz.Quad(points[i * 4 : i * 4 + 4]).rect

        words = [w for w in wordlist if fitz.Rect(w[:4]).intersects(r)]
        sentences.append(" ".join(w[4] for w in words))

    pno = re.findall(r'^\D*(\d+)', str(annot))
    pno = "#".join(pno)
    sentences.append(pno)
    sentences.append(annot.colors["stroke"])
    return sentences

# Function to handle multiple pages
def handle_page(page):
    wordlist = page.get_text("words")  # list of words on page
    wordlist.sort(key=lambda w: (w[3], w[0]))  # ascending y, then x
    highlights = []
    annot = page.first_annot
    while annot:
        if annot.type[0] == 8:
            highlights.append(_parse_highlight(annot, wordlist))
        annot = annot.next
    return highlights

# Function to extract highlights from a PDF file
def extract_highlights(filepath: str) -> List:
    doc = fitz.open(filepath)
    highlights = []
    for page in doc:
        highlights += handle_page(page)
    return highlights

# Function to clean the text in 'db' variable
def clean_text(db):
    db = re.sub(r"\([^()]*\b\d+\b[^()]*\)", "", db)  # Remove text within parentheses containing digits    db = re.sub(r"\\[a-z][0-9]+", "", db)
    db = re.sub(r"(?:'|\")www.cairn.info(?:'|\")", "", db)
    db = re.sub(r"\\\\xa[0-9]+", "", db)
    db = re.sub(r"(?:'|\")téléchargé", "", db)
    db = re.sub(r"[0-9]{2}/[0-9]{2}/[0-9]{4}", "", db)
    db = re.sub(r"'?Document[[:blank:]]?'?", "", db)
    db = re.sub(r"[0-9]{3}\.[0-9]{2}\.[0-9]{3}\.[0-9]{3}", "", db)
    db = re.sub(r"[0-9]{3}\.[0-9]{3}\.[0-9]{2}\.[0-9]{3}", "", db)

    db = re.sub(r"(\\[[^\\]]*?\\])", "\n\\1\\\n", db)

    db = re.sub(r"\\n, '", "\\\nTitre : ", db)
    db = re.sub(r"(0\\)\\])(\\n\\], ')", "\\1\\\n Titre : ", db)

    db = re.sub(r"\\[\\]", "", db)
    db = re.sub(r"^\\['?", "", db)
    db = re.sub(r",\\]", "", db)
    db = re.sub(r", '", "", db)
    db = re.sub(r"( |)Normale'", "", db)
    db = re.sub(r"( |)Téléchargé'", "", db)
    db = re.sub(r" Paris'", "", db)
    db = re.sub(r" Ecole('| )", "", db)
    db = re.sub(r"&dquo;", "", db)

    return db

# Main function to extract highlights from all PDFs in the current directory
def main():
    pdf_files = [f for f in os.listdir('.') if f.endswith('.pdf')]
    highlightss = []

    for filename in pdf_files:
        highlights = extract_highlights(filename)
        highlightss.append((filename, highlights))

    # Write the extracted highlights to a file named 'highlights_output.txt'
    with open("highlights_output.txt", "w", encoding="UTF-8") as myfile:
        for filename, highlights in highlightss:
            myfile.write(f"File: {filename}\n")
            for highlight in highlights:
                myfile.write(f"{highlight}\n")

if __name__ == "__main__":
    main()
